import java.util.Scanner;

public class mathe {
	
	public static double quadrat(double x) {
		
		double ergebnis = x * x;
		return ergebnis;
	}

	public static void main(String[] args) {
		
		Scanner meinScanner = new Scanner(System.in);
		
		double x=0.0;
		double quadratwert=0.0;
		
		
		
		System.out.print("Wie lautet der Wert: "); 
	    x = meinScanner.nextDouble();
		
	    quadratwert = quadrat(x);
	    
	    System.out.printf("Der Quadratwert von %.2f ist %.2f\n", x, quadratwert);
		
	}

}
