import java.util.Scanner; // 1

public class Test {
	
	// Methode "verarbeitung"
	public static double verarbeitung(double pZahl1, double pZahl2) {
		
		double ergebnis = (pZahl1 + pZahl2) / 2;
		return ergebnis;
	}
	
	public static void main(String[] args) {
		
		Scanner meinScanner = new Scanner(System.in); // 2
		
		  // Deklaration der Variablen
	      double zahl1=0.0;
	      double zahl2=0.0;
	      double mittelwert=0.0;
	      
	      // (E) "Eingabe"
	      System.out.print("Wie lautet der erste Wert: "); 
	      zahl1 = meinScanner.nextDouble();
	      
	      System.out.print("Wie lautet der zweite Wert: "); 
	      zahl2 = meinScanner.nextDouble(); // 3
	      
	      // (V) Verarbeitung
	      
	      // mittelwert = (zahl1 + zahl2) / 2.0; (alt, ohne Methode)
	      
	      mittelwert = verarbeitung(zahl1, zahl2);
	      
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, mittelwert);
	      
	      
	   }
	}
