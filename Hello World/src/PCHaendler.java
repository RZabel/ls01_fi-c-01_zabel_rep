import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text1) {
		String eingabe1 = text1;
		return eingabe1;
	}
	
	public static int liesInt(int text2) {
		int eingabe2 = text2;
		return eingabe2;
		
	}
	
	public static double liesDouble(double text3) {
		double eingabe3 = text3;
		return eingabe3;
		
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double berechnung1 = anzahl * nettopreis;
		return berechnung1;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double berechnung2 = nettogesamtpreis * (1+ mwst / 100);
		return berechnung2;	
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		String artikel1;
		int anzahl1;
		double netto1;
		double rechnung1;
		double rechnung2;

		// Benutzereingaben lesen
		System.out.println("was möchten Sie bestellen?");
		String artikel = myScanner.next();
		artikel1 = liesString(artikel);

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		anzahl1 = liesInt(anzahl);
		

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		netto1 = liesDouble(preis);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		
		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}