﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static double fahrkartenbestellungErfassen(double aZahl1, int aZahl2) {
		return aZahl1 * aZahl2;
	}
	
	public static double fahrkartenBezahlen(double bZahl1) {
		double bezahlt = 0.0;
		double eingeworfen = 0.0;
		double ergebnis2 = 0.0;
		Scanner tastatur2 = new Scanner(System.in);
		while(bezahlt < bZahl1) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (bZahl1 - bezahlt));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 EURO): ");
			eingeworfen = tastatur2.nextDouble();
			bezahlt += eingeworfen;
		}
		ergebnis2 = bezahlt - bZahl1;
		return ergebnis2;
	}
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag; // float
       double eingeworfeneMünze; // float
       double rückgabebetrag;
       int anzahlTickets;// int, da Ticketanzahl immer Ganzzahl
       double erfassung;
       double bezahlung; // float

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextInt();
       
       // zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
       erfassung = fahrkartenbestellungErfassen(zuZahlenderBetrag, anzahlTickets);
       // Nummerische Promotion bei Berechnung
       // zunächst Regel 2; int wird zu double
       // dann Regel 4; Ergebnis hat selben Datentypen, wie beförderter Wert, also double
       

       // Geldeinwurf
       // -----------
       bezahlung = 0.0;
       eingezahlterGesamtbetrag = 0.0;
       //while(bezahlung < erfassung)
       //{
    	   //System.out.printf("Noch zu zahlen: %.2f EURO\n",(erfassung - bezahlung));
    	   //System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   //eingeworfeneMünze = tastatur.nextDouble();
    	   
    	   bezahlung = fahrkartenBezahlen(erfassung);
    	   //eingezahlterGesamtbetrag += eingeworfeneMünze;
       //}

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (float f = 0; f < 8; f++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = bezahlung;
       Math.round((rückgabebetrag * 100.0) / 100.0);
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2d EURO\n", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt: ");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}