package raumSchiff;

import java.util.ArrayList;

public class Raumschiff {
	
	private String schiffsname;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int photonentorpedoAnzahl;
	private int androidenAnzahl;
	private ArrayList <String> broadcastKommunikator;
	private ArrayList <String> ladungsverzeichnis;
	
	
	public Raumschiff(String schiffsname, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
			int lebenserhaltungssystemeInProzent, int photonentorpedoAnzahl, int androidenAnzahl) {
		setSchiffsname(schiffsname);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setAndroidenAnzahl(androidenAnzahl);
	}
	
	public Raumschiff(String schiffsname) {
		setSchiffsname(schiffsname);
	}
	
	
	public void setSchiffsname (String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setEnergieversorgungInProzent (int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setSchildeInProzent (int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setHuelleInProzent (int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent (int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setPhotonentorpedoAnzahl (int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setAndroidenAnzahl (int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void addLadung(Ladung neueLadung) {
		
	}
	
}
