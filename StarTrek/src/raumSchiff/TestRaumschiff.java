package raumSchiff;

public class TestRaumschiff {
	
	public static void main(String[] args) {
		
		Raumschiff rs1 = new Raumschiff ( "Enterprise", 100, 100, 100, 100, 1, 3 );
		Raumschiff rs2 = new Raumschiff ( "Borg", 120, 100, 100, 100, 0, 3 );
		
		
		System.out.println("Schiffsname: " + rs1.getSchiffsname());
		System.out.println("Energieversorgung: " + rs1.getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + rs1.getSchildeInProzent() + "%");
		System.out.println("Hülle: " + rs1.getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme: " + rs1.getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Anzahl Photonentorpedos: " + rs1.getPhotonentorpedoAnzahl());
		System.out.println("Anzahl Androiden: " + rs1.getAndroidenAnzahl());
		System.out.println("\nSchiffsname: " + rs2.getSchiffsname());
		System.out.println("Energieversorgung: " + rs2.getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + rs2.getSchildeInProzent() + "%");
		System.out.println("Hülle: " + rs2.getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme: " + rs2.getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Anzahl Photonentorpedos: " + rs2.getPhotonentorpedoAnzahl());
		System.out.println("Anzahl Androiden: " + rs2.getAndroidenAnzahl());
		
	}
	
}
