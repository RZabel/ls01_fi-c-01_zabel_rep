package raumSchiff;

public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	public int getMenge() {
		return this.menge;
	}

}
